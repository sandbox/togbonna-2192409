<?php
/* @file
 * API and hooks documentation for the Drupal Commerce Interswitch WebPAY module.
 */

/**
 * Alter payment data before it is sent to Interswitch WebPAY.
 *
 * Allows modules to alter the payment data before the data is signed and sent
 * to Interswitch WebPAY.
 *
 * @param &$data
 *   The data that is to be sent to Interswitch WebPAY as an associative array.
 * @param $order
 *   The commerce order object being processed.
 * @param $settings
 *   The configuration settings.
 *
 * @return
 *   No return value.
 */
function hook_drupal_commerce_interswitch_webpay_data_alter(&$data, $order, $settings) {
  // Data alterations here.
}
